const debug = require('debug')("APP:user.model")



const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Order }) {
      this.hasMany(Order, {foreignKey: 'user_id', as: 'orders'})
    }

    static async findById(id) {
      debug(`Find by id: ${id}`)
      const cond = { where : { id }}
      return await this.findOne(cond)
    }

    static async findByEmail(email) {
      debug(`Find by email: ${email}`)
      const cond = {
        where: { email }
      }
      return await this.findOne(cond)
    }

    static async findByUsername(username) {
      debug(`find by username ${username}`)
      const cond = {
        where: { username }
      }
    
      const user = await this.findOne(cond)
      debug(`User found ${JSON.stringify(user)}`)
      return user
    }

    static async autheticate(username, password) {
      let user = await this.findByUsername(username)
      if(!user) throw new Error('Autenticacion fallida: nombre de usuario no encontrado en la base de datos')
      debug(`User found in the database, Proceeding to validate the password`)
      if(user.password !== password) throw new Error('Autenticacíon fallida: Contraseña incorrecta')
      debug('Correct autentication')
      return user
    }

    toJSON() {
      return {...this.get(), password: undefined, createdAt: undefined, updatedAt: undefined}
    }

  };
  User.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: { msg: 'Ingrese un email válido'}
      }
    },
    is_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    telephone: {
      type: DataTypes.STRING,
      allowNull: false
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users'
  })
  return User
}