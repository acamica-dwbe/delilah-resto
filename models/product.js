const debug = require('debug')("APP:product.model")


const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Product_order, Order }) {
      this.belongsToMany(Order, {through: Product_order, foreignKey: 'product_id'})
      this.hasMany(Product_order, { foreignKey: 'product_id' })
    }

    static async findByName(name) {
      debug(`Find by name ${name}`)
      const cond = {
        where: { name }
      }
      return await this.findOne(cond)
    }

    static async findById(id) {
      debug(`Find by id: ${id}`)
      const cond = { where : { id }}
      return await this.findOne(cond)
    }

    toJSON() {
      return {...this.get(), quantity: undefined, createdAt: undefined, updatedAt: undefined}
    }

  };
  Product.init({
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isNumeric: {
          args: true,
          msg: "La cantidad debe ser un valor númerico"
        }
      }
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      validate: {
        isNumeric: true
      }
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Product',
    tableName: 'products'
  })
  return Product
}