'use strict';
const debug = require('debug')('APP:order.model')

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ User, Payment_method, Product_order, Product }) {
      this.belongsTo(User, {foreignKey: 'user_id'})
      //this.hasOne(Payment_method, {foreignKey: 'id_payment_method'})
      this.belongsToMany(Product, {through: Product_order, foreignKey: 'order_id'})
      this.hasMany(Product_order, { foreignKey: 'order_id', as: 'products' })
    }

    static async findById(id) {
      debug(`Find by id: ${id}`)
      const cond = { where : { id }}
      return await this.findOne(cond)
    }

    toJSON() {
      return {...this.get(), createdAt: undefined, updatedAt: undefined}
    }

  };
  Order.init({
    shipping_addresss: {
      type: DataTypes.STRING,
      allowNull: true
    },
    id_payment_method: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    total: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'created'
    },
  }, {
    sequelize,
    modelName: 'Order',
    tableName: 'orders'
  });
  return Order;
};