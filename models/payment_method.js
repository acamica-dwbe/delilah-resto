const debug = require('debug')("APP:payment_method.model")

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Payment_method extends Model {
    static associate({ Order }) {
      this.hasMany(Order, {foreignKey: 'id_payment_method'})
    }

    static async findById(id) {
      debug(`Find by id: ${id}`)
      const cond = { where : { id }}
      return await this.findOne(cond)
    }

    static async findByType(type) {
      debug(`Find by type: ${type}`)
      const cond = {
        where: {type}
      }

      return await this.findOne(cond)
    }

    toJSON() {
      return {...this.get(), createdAt: undefined, updatedAt: undefined}
    }

  };
  Payment_method.init({
    type: {
      type: DataTypes.STRING,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'Payment_method',
    tableName: 'payment_methods'
  });
  return Payment_method
}