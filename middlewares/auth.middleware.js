import { User } from "../models"
import Debug from 'debug'
const debug = Debug("APP:middleware:auth")

const isAutheticated = async (req, res, next) => {
  const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
  const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':')

  if (username && password) {
    debug(`Username: ${username}, Password: ${password}`)
    // Procedo a validar las credenciales de autenticación
    try {
      let user = await User.autheticate(username, password)
      debug(`Usuario autenticado, se procede al siguiente middleware`)
      req.user = user
      return next()
    } catch(error) {
      return res.status(401).send({errors: {error: error.message} }) 
    }
  }

  return res.status(401).send({errors: {error: 'Necesita pasar las credenciales de autenticación para acceder al recurso'} }) 
}

const onlyAdmins = (req, res, next) => {
  debug("is admin?")
  debug(`req.user ${JSON.stringify(req.user)}`)
  if(!req.user || !req.user.is_admin) return res.status(401).send({errors: {error: "Necesita ser un administrador para realizar la acción"}})
  next()
}

module.exports = {
  isAutheticated,
  onlyAdmins
}