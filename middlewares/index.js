const {logger} = require('./customs.middleware')
const { validateRequest } = require('./validateRequest.middleware')
const { isAutheticated, onlyAdmins} = require('./auth.middleware')

const middlewares = {
  LOGGER: logger,
  validateRequest,
  isAutheticated,
  onlyAdmins
}

module.exports = middlewares