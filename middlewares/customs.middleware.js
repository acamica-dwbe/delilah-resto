import Debug from 'debug'
const debug = Debug('APP:customsMiddlewares')

const logger = (req, res, next) => {
  debug(`request HTTP method: ${req.method}, request Path: ${req.path}`)
  next()
}

module.exports = { logger }