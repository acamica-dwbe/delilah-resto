import Debug from 'debug'
const debug = Debug('APP:validationResult')
import {validationResult} from 'express-validator'

exports.validateRequest = (req, res, next) => {
  debug('Validate Request')
  const errors = validationResult(req)
  if(!errors.isEmpty()) {
    debug(`Errors: ${JSON.stringify(errors)}`)
    return res.status(400).json({ errors: errors.array() })
  }
  next()
} 