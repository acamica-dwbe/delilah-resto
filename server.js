import chalk from 'chalk'
import Debug from 'debug'
const debug = Debug('APP:server')

import app from './app'

const { sequelize } = require('./models')

app.listen(app.get('port'), async ()=>{
  debug(`Servidor escuchando el el puerto ${chalk.greenBright(app.get('port'))}`)
  await sequelize.authenticate()
  debug(`Connected to database`)
})
