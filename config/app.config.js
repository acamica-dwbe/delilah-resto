import Debug from 'debug'
const debug = Debug('APP:config')
/**
 * Module to set de default configurations to the app
 */
module.exports = (app) => {
  debug('Setting configuration')
  app.set('port', process.env.PORT || 3000)
  app.set('environment', process.env.ENVIRONMENT || 'development')
}