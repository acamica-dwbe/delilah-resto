import express from 'express'
import {checkSchema} from 'express-validator'

import { validateRequest } from '../middlewares'

// Controllers
import { login, signup } from '../controllers/auth.controllers'
import { User } from '../models'
const router = express.Router()

/**
 * @swagger
 * security:
 *   - basicAuth: []
 * components:
 *   securitySchemes:
 *     basicAuth:
 *       type: http
 *       scheme: basic
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: Nombre del Usuario.
 *           example: Admin
 *         email: 
 *           type: string
 *           description: Email del Usuario
 *           example: admin@email.com
 *         is_admin:
 *           type: boolean
 *           description: Indica si el usuario es administrador o no
 *           example: true
 *         telephone:
 *           type: string
 *           description: Teléfono del Usuario
 *           example: 555-55-55
 *         username:
 *           type: string
 *           description: Nickname del usuario
 *           example: admin
 *         address:
 *           type: string
 *           description: Direccion del usuario
 *           example: Calle 10 Nro 50-55
 *     UserResponse:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: identificador del usuario en la base de datos
 *               example: 1
 *             createdAt:
 *               type: date
 *               description: fecha de creación del usuario
 *               example: 2021-05-15 15:36:24.669 +00:00
 *         - $ref: '#/components/schemas/User'
 *     NewUser:
 *       allOf:
 *         - type: object
 *           properties:
 *             password:
 *               type: string
 *               description: contraseña del usuario.
 *               example: 123456
 *         - $ref: '#/components/schemas/User'
 *     LoginUser:
 *       allOf:
 *         - type: object
 *           properties:
 *             username:
 *               type: string
 *               description: nickname del usuario
 *               example: admin
 *             password:
 *               type: string
 *               description: contraseña del usuario.
 *               example: 123456
 * 
 */

/**
 * @swagger
 * /api/v1/auth/login:
 *   post:
 *     summary: Loguea un usuario en la aplicacion.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LoginUser'
 *     responses:
 *       200:
 *         description: Autenticacion correcta
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/UserResponse'
*/
router.post('/login', checkLogin(), validateRequest, login)

/**
 * @swagger
 * /api/v1/auth/signup:
 *   post:
 *     summary: Registra a un usuario en la aplicacion.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewUser'
 *     responses:
 *       200:
 *         description: Registo correcto
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/UserResponse'
*/
router.post('/signup', checkSignup(), validateRequest, signup)

module.exports = router

function checkLogin() {
  return checkSchema({
    username: {
      isEmpty: {
        errorMessage: "Ingrese el nombre de usuario",
        negated: true
      }
    },
    password: {
      isEmpty: {
        errorMessage: "Ingrese la contraseña",
        negated: true
      }
    }
  })
}


function checkSignup() {
  return checkSchema({
    password: {
      isLength: {
        errorMessage: 'La contraseña debe tener al menos 7 caracteres',
        options: { min: 7}
      }
    },
    email: {
      isEmail: {
        errorMessage: 'Ingrese un correo electronico válido',
        bail: true
      },
      custom: {
        options: async(value) => {
          if(await User.findByEmail(value)) {
            throw new Error('Ya existe un usuario con ese correo electronico')
          }
          return true
        }
      }
    },
    username: {
      isLength: {
        errorMessage: 'El nombre de usuario debe tener al menos 6 caracteres y como máximo 10',
        options: { min: 7, max: 10}
      },
      custom: {
        options: async(value) => {
          if(await User.findByUsername(value)) throw new Error(`Ya existe un usuario con el nombre de usuario: ${value}`)
          return true
        }
      }
    },
    name: {
      isEmpty: {
        errorMessage: 'El nombre del usuario no puede estar vacio',
        negated: true
      }
    },
    telephone: {
      isEmpty: {
        errorMessage: 'Ingrese el número de Teléfono',
        negated: true
      }
    },
    address: {
      isEmpty: {
        errorMessage: 'Ingrese su dirección',
        negated: true
      }
    }
  })
}