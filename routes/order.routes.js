import express from 'express'
import { checkSchema } from 'express-validator'

import { OrderController } from '../controllers'
import { Payment_method, User, Product, Order} from '../models'


import { isAutheticated, validateRequest, onlyAdmins } from '../middlewares'

const router = express.Router()

router.post('/new', validateNewOrderReques(), validateRequest, isAutheticated, OrderController.createNewOrder)

router.get('/my_orders', isAutheticated, OrderController.getOrderHistory)

router.put('/confirm/:id', isAutheticated, validateChangeOrder(), validateRequest , OrderController.closeOrder)

router.put('/edit/:id', isAutheticated, validateChangeOrder(), validateRequest, OrderController.editOrder)

router.get('/admin/all-orders', isAutheticated, onlyAdmins, OrderController.getAllOrders)

router.patch('/admin/edit-order-state/:id', isAutheticated, onlyAdmins, OrderController.updateState)



module.exports = router

function validateChangeOrder() {
  return checkSchema({
    id: {
      in: ['params'],
      isEmpty: {
        errorMessage: "Indique el id del pedido que quiere modificar",
        negated: true
      },
      custom: {
        options: async(value, { req }) => {
          let order = await Order.findById(value)
          if(!order) throw new Error(`Pedido con id ${value}, no se encuentra registrado en la base de datos`)
          if(order.user_id !== req.user.id) throw new Error(`No tiene permiso de modificar el pedido con id ${value}, ya que este no esta registrado a su nombre`)
          if(order.estadπoo !== 'pendiente') throw new Error(`El pedido ya ha sido confirmado, no lo puede modificar. Estado acutal del pedido: ${order.estado}`)
          return true
        }
      }
    },
    shipping_addresss: {
      optional: { options: { nullable: true } },
      isString: {
        errorMessage: "La dirección de envío debe ser un String",
        bail: true
      }
    },
    id_payment_method: {
      optional: { options: { nullable: true } },
      custom: {
        options: async(value) => {
          if(! await Payment_method.findById(value)) throw new Error(`No existe un metodo de pago con id ${value}`)
          return true
        }
      }
    },
    products: {
      isArray: {
        errorMessage: "El campo products debe ser un array con el id de los productos y la candidad a ser añadidos al pedido",
        bail: true
      },
      optional: { options: {nullable: true}}
    },
    'products.*.product_id': {
      isEmpty: {
        errorMessage: "Debe ingresar el id del producto",
        negated: true
      },
      custom: {
        options: async(value)  => {
          if(!await Product.findById(value)) throw new Error(`El producto con id: ${value} no se encuentra registrado en la base de datos`)
          return true
        }
      }
    },
    'products.*.quantity': {
      isEmpty: {
        errorMessage: 'Debe ingresar la cantidad del producto',
        negated: true
      },isNumeric: {
        errorMessage: 'La cantidad del producto debe ser un número',
        bail: true
      }
    }
  })
}

function validateNewOrderReques() {
  return checkSchema({
    id_payment_method: {
      isEmpty: {
        errorMessage: "Indique el medio de pago, para ver los medios de pago disponibles -> /api/v1/payment_methods",
        negated: true
      },
      custom: {
        options: async(value) => {
          if(! await Payment_method.findById(value)) throw new Error(`No existe un metodo de pago con id ${value}`)
          return true
        }
      }
    },
    products: {
      isArray: {
        errorMessage: "El campo products debe ser un array con el id de los productos y la candidad a ser añadidos al pedido",
        bail: true
      },
      optional: { options: {nullable: true}}
    },
    'products.*.product_id': {
      isEmpty: {
        errorMessage: "Debe ingresar el id del producto",
        negated: true
      },
      custom: {
        options: async(value)  => {
          if(!await Product.findById(value)) throw new Error(`El producto con id: ${value} no se encuentra registrado en la base de datos`)
          return true
        }
      }
    },
    'products.*.quantity': {
      isEmpty: {
        errorMessage: 'Debe ingresar la cantidad del producto',
        negated: true
      },isNumeric: {
        errorMessage: 'La cantidad del producto debe ser un número',
        bail: true
      }
    }
  })
}