import express from 'express'

import { UserController } from '../controllers'

import {isAutheticated, onlyAdmins} from '../middlewares'

const router = express.Router()

/**
 * @swagger
 * /api/v1/users:
 *   get:
 *     security:
 *       - basicAuth: []
 *     summary: Listar todos los Usuarios.
 *     description: Retorna todos los usuarios de la base de datos
 *     responses:
 *       200:
 *         description: Lista de Usuarios.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/UserResponse'
 */
router.get('/', isAutheticated, onlyAdmins, UserController.getAllUsers)

module.exports = router