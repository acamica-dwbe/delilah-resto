import express from 'express'
import { checkSchema } from 'express-validator'

import { Product } from '../models'
import { ProductController } from '../controllers'
import { 
  validateRequest,
  isAutheticated, 
  onlyAdmins
} from '../middlewares'


const router = express.Router()
// api/v1/products/new
/**
 * @swagger
 * components:
 *   schemas:
 *     NewProduct:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: Nombre del producto.
 *           example: Zanahoria
 *         price: 
 *           type: float
 *           description: Precio del producto
 *           example: 2000
 *         quantity:
 *           type: integer
 *           description: Cantidad disponible del producto
 *           example: 10
 *         category:
 *           type: string
 *           description: Categoria a la que pertenece el producto
 *           example: Verduras
 *     Product:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: Id del Producto.
 *               example: 0
 *         - $ref: '#/components/schemas/NewProduct'
 */

/**
 * @swagger
 * /api/v1/products:
 *   get:
 *     summary: Listar todos los productos.
 *     description: Retorna la lista de productos disponibles para ser agregados a los pedidos.
 *     responses:
 *       200:
 *         description: Lista de productos.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Product'
*/
router.get('/', ProductController.getAllProducts)
//https://stackoverflow.com/a/51756715

/**
 * @swagger
 * /api/v1/products/new:
 *   post:
 *     security:
 *       - basicAuth: []
 *     summary: Registra un nuevo producto en la base de datos.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewProduct'
 *     responses:
 *       200:t
 *         description: Registo correcto del producto
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/Product'
*/
router.post('/new',checkProduct(), validateRequest, isAutheticated, onlyAdmins, ProductController.createNewProduct)

router.put('/edit/:id', checkProductExist(), validateRequest, isAutheticated, onlyAdmins, ProductController.editProduct)

router.delete('/delete/:id', checkProductExist(), validateRequest, isAutheticated, onlyAdmins, ProductController.deleteProduct)

module.exports = router

function checkProductExist() {
  return checkSchema({
    id: {
      in: ['params'],
      custom: {
        options: async(value) => {
          if(!await Product.findById(value)) throw new Error(`No existe un producto registrado con id ${value}`)
          return true
        }
      }
    }
  })
}

function checkProduct() {
  return checkSchema({
    name: {
      isEmpty: {
        errorMessage: "Ingrese el nombre del producto",
        negated: true
      },
      custom: {
        options: async(value) => {
          if(await Product.findByName(value)) throw new Error(`El producto ya existe, para modificarlo utilice la ruta apropiada`)
          return true
        }
      }
    },
    quantity: {
      isEmpty: {
        errorMessage: "Ingrese la cantidad del producto",
        negated: true
      },isNumeric: {
        errorMessage: "La cantidad debe ser un número",
        bail: true
      }
    },
    price: {
      isEmpty: {
        errorMessage: "Ingrese el precio del producto",
        negated: true
      },isNumeric: {
        errorMessage: "El precio debe ser un número",
        bail: true
      }
    },
    category: {
      isEmpty: {
        errorMessage: "Ingrese la categoria del producto",
        negated: true
      }
    }
  })
}