import express from 'express'
import { checkSchema } from 'express-validator'

import { Payment_method } from '../models'
import { PaymentMethodController } from '../controllers'

import {isAutheticated, onlyAdmins, validateRequest} from '../middlewares'


const router = express.Router()

router.get('/', isAutheticated, PaymentMethodController.getAllPaymentMethods)

router.put('/edit/:id', validatePaymentMethodEdition(), validateRequest,  isAutheticated, onlyAdmins, PaymentMethodController.modifyPaymentMethod)

router.post('/new', validatePaymentMethod(), validateRequest, isAutheticated, onlyAdmins, PaymentMethodController.createPaymentMethod)

router.delete('/delete/:id', validateDeletePaymentMethodRequest(), validateRequest, isAutheticated, onlyAdmins, PaymentMethodController.deletePaymentMethod)

module.exports = router

/**
 * Válida el request cuando un admin quiere borrar un metodo de pago
 */
function validateDeletePaymentMethodRequest() {
  return checkSchema({
    id: {
      in: ['params'],
      errorMessage: 'Ingrese el id del metodo de pago que quiere modificar en los parametros de la url',
      custom: {
        options: async(value) =>{
          if(!await Payment_method.findOne({where: {id: value}})) throw new Error(`No existe un metodo de pago registrado en la base de datos con id ${value}`)
          return true
        }
      }
    }
  })
}

/**
 * Valida el request cuando un admin quiere modificar un metodo de pago 
 */
function validatePaymentMethodEdition() {
  return checkSchema({
    id: {
      in: ['params'],
      errorMessage: 'Ingrese el id del metodo de pago que quiere modificar en los parametros de la url',
      custom: {
        options: async(value) =>{
          if(!await Payment_method.findOne({where: {id: value}})) throw new Error(`No existe un metodo de pago registrado en la base de datos con id ${value}`)
          return true
        }
      }
    },
    type: {
      isEmpty: {
        errorMessage: "Ingrese el Tipo del metodo de pago",
        negated: true
      },
      custom: {
        options: async(value) => {
          if(await Payment_method.findByType(value)) throw new Error(`Ya existe un metodo de pago con valor ${value}, Si lo que busca es editarlo ingrese un valor diferente al ya existente para el tipo de metodo de pago`)
          return true
        }
      }
    }
  })
}

/**
 * Valida el request cuando un admin quiere creat un nuevo metodo de pago
 */
function validatePaymentMethod() {
  return checkSchema({
    type: {
      isEmpty: {
        errorMessage: "Ingrese el Tipo del metodo de pago",
        negated: true
      },
      custom: {
        options: async(value) => {
          if(await Payment_method.findByType(value)) throw new Error(`Ya existe un metodo de pago con valor ${value}`)
          return true
        }
      }
    }
  })
}