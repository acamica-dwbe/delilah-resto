import express from 'express'

const router = express.Router()

// use all the routes

router.use('/auth', require('./auth.routes'))
router.use('/products', require('./product.routes'))
router.use('/users', require('./user.routes'))
router.use('/payment_methods', require('./payment_methods.routes'))
router.use('/orders', require('./order.routes'))

module.exports = router
