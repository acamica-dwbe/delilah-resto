
const controllers = {
  UserController: require('./user.controllers'),
  ProductController: require('./product.controller'),
  AuthController: require('./auth.controllers'),
  PaymentMethodController: require('./paymet_methods.controller'),
  OrderController: require('./order.controller')
}

module.exports = controllers