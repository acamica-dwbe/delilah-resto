import Debug from 'debug'
const debug = Debug('APP:order.controller')

import { Product, Order, Product_order } from '../models'

const ORDER_STATES =  ['pendiente', 'confirmado', 'en preparacion', 'enviado', 'entregado']

async function updateState(req, res) {
  try {
    let state = req.body.state
    if(!state) return res.status(400).send({ errors: [ { error: 'Suministre el estado al que quiere cambiar la orden'}]})
    let index = ORDER_STATES.findIndex(orderState => orderState === state)
    if (index < 0) {return res.status(400).send({errors: [ {error: `El estado suministrado: ${state}, no es un estado válido... Ingrese uno de los siguientes estados ${ORDER_STATES}`}]})}
    const order = await Order.findById(req.params.id)
    debug(`Order: ${JSON.stringify(order)}`)
    await order.update({estado: state})
    return res.status(200).send({order})
  } catch (error) {
    return res.status(500).send(error)
  }
}

async function getAllOrders(req, res) {
  try {
    const allOrders = await Order.findAll({
      include: {
        model: Product_order,
        as: 'products',
        include:[{
            model: Product
          }
        ]
      }
    })
    return res.status(200).send({order: allOrders})
  } catch(error) {
    return res.status(500).send(error)
  }
}

async function editOrder(req, res) {
  try {
    let order = await Order.findById(req.params.id)
    debug(`Order to edit: ${JSON.stringify(order)}`)

    // If I enter new products, I have to recalculate the total
    let products = req.body.products
    let total = order.total || 0
    debug(`Total before: ${total}`)
    if(products && Array.isArray(products) && products.length > 0 ) {
      let newProducts = []
      for (const productInfo of products) {
        // Check if is an already existing product and is just changing the quantity
        let existingProductOrder = await Product_order.findOne({ where: {
          product_id: productInfo.product_id,
          order_id: order.id
        }})
        let product = await Product.findById(productInfo.product_id)
        debug("Product")
        debug(product)
        if(existingProductOrder) {
          total -=  (existingProductOrder.quantity * product.price)
          await existingProductOrder.update({quantity: productInfo.quantity})
          total += (productInfo.quantity * product.price)
        } else {
          newProducts.push({...productInfo})
          total += (productInfo.quantity * product.price)
        }
      }
      if (newProducts.length > 0) {
       let  productsOrders = newProducts.map(productOrder => ({...productOrder, order_id: order.id}))
        await Product_order.bulkCreate(productsOrders)
      }
    }
    debug(`TOTAL: ${total}`)
    await order.update({...req.body, total})
    const editedOrder = await Order.findOne({
      where: { id: order.id },
      include: {
        model: Product_order,
        as: 'products',
        include:[{
            model: Product
          }
        ]
      }
    })
    return res.status(200).send({order: editedOrder})
  } catch(error) {
    debug(`Error editing order ${JSON.stringify(error)}`)
    debug(error.message)
    return res.status(500).send(error)
  }
}

async function closeOrder(req, res) {
  debug(`Closing order with id ${req.params.id}`)
  try {
    let order = await Order.findById(req.params.id)
    debug(`Order to close ${JSON.stringify(order)}`)
    await order.update({estado: 'confirmado'})
    return res.status(200).send('Pedido confimado')
  }catch(error) {
    debug(JSON.stringify(error))
    return res.status(500).send(error)
  }
}

async function  getOrderHistory(req, res) {
  debug(`Getting order hystory for user: ${req.user.id}`)
  try {
    const allOrdersFromUser = await Order.findAll({
      where: { user_id: req.user.id },
      include: {
        model: Product_order,
        as: 'products',
        include:[{
          model: Product
        }]
      }
    })
    debug(`All orders: ${JSON.stringify(allOrdersFromUser)}`)
    return res.status(200).send({orders: allOrdersFromUser})
  } catch(error) {
    return res.status(500).send(error)
  }

}

async function createNewOrder(req, res){ 
  debug(`Creating a new order ${JSON.stringify(req.body)}`)
  debug(`New order for the user with id ${req.user.id}`)
  try {
    let newOrder = {
      shipping_addresss: req.user.address,
      ...req.body,
      user_id: req.user.id,
      total: 0,
      estado: 'pendiente' // Initial state
    }
  
    let products = req.body.products
    let productsOrders = []
  
    if(products && Array.isArray(products) && products.length > 0 ) {
      delete newOrder.products
      for (const productInfo of products) {
        let product = await Product.findById(productInfo.product_id)
        if(product) {
          let productOrder = {
            ...productInfo,
          }
          debug(`productOrder ${JSON.stringify(productOrder)}`)
          productsOrders.push(productOrder)
          let productOrderTotal = product.price * productOrder.quantity
          newOrder.total = newOrder.total + productOrderTotal
        }
      }
    }
  
    debug(`new Order: ${JSON.stringify(newOrder)}`)
    // Create the new order
    let order = await Order.create({...newOrder})
    debug(`Products Orders out: ${JSON.stringify(productsOrders)}`)
    
    if(productsOrders.length > 0) {
      // Create the product orders
      productsOrders = productsOrders.map(productOrder => ({...productOrder, order_id: order.id}))
      debug(`Products Orders in: ${JSON.stringify(productsOrders)}`)
      await Product_order.bulkCreate(productsOrders)
    }

    const orderWithProducts = await Order.findOne({
      where: { id: order.id },
      include: {
        model: Product_order,
        as: 'products',
        include:[{
            model: Product
          }
        ]
      }
    })
  
    return res.status(201).send({order: orderWithProducts})
  } catch(error) {
    return res.status(500).send(error)
  }
}

module.exports = {
  createNewOrder,
  getOrderHistory,
  closeOrder,
  editOrder,
  getAllOrders,
  updateState
}