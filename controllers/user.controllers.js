import Debug from 'debug'
const debug = Debug("APP:user.controller")

import { User } from '../models'

async function getAllUsers(req, res) {
  debug('Get all users')
  const users = await User.findAll()
  debug(`Users: ${JSON.stringify(users)}`)
  return res.status(200).send({users})
}

module.exports = {
  getAllUsers
};