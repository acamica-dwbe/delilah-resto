import Debug from 'debug'
const debug = Debug('APP:product.controller')

import { Product } from '../models'

async function getAllProducts(req, res) {
  debug('Find all products')
  let products = await Product.findAll()
  debug(`All products: ${JSON.stringify(products)}`)
  return res.status(200).send({products})
}

async function createNewProduct(req, res) {
  debug(`Create new Product ${req.body}`)
  let product = await Product.create({...req.body})
  return res.status(201).send({product})
}

async function editProduct(req, res) {
  debug(`Edit product id: ${req.params.id}, body: ${req.params.body}`)
  try {
    let product = await Product.findById(req.params.id)
    debug(`Product to modify ${JSON.stringify(product)}`)
    await product.update({...req.body})
    return res.status(200).send({product})
  } catch(error) {
    return res.status(500).send({errors: error.message.split('\n')})
  }
}

async function deleteProduct(req, res) {
  debug(`Deleting product with id: ${req.params.id}`)
  try {
    await Product.destroy({ where: { id: req.params.id }})
    return res.status(204).send()
  } catch(error) {
    return res.status(500).send({errors: [{ error: error.message }]})
  }
}

module.exports = {
  getAllProducts,
  createNewProduct,
  editProduct,
  deleteProduct
}