import Debug from 'debug'
const debug = Debug('APP:payment_method.controller')

import { Payment_method } from '../models'

async function getAllPaymentMethods(req, res) {
  try {
    debug('Get all payment methods')
    const paymentMethods = await Payment_method.findAll()
    debug(`Payment_methods ${JSON.stringify(paymentMethods)}`)
    return res.status(200).send({paymentMethods})
  } catch(error) {
    debug(`Error ${JSON.stringify(error)}`)
    return res.status(500).send('Problemas internos')
  }
  
}

async function createPaymentMethod(req, res) {
  debug(`Create payment method ${JSON.stringify(req.body)}`)
  let paymentMethod = await Payment_method.create({...req.body})
  return res.status(201).send(paymentMethod)
}

async function modifyPaymentMethod(req, res) {
  debug(`Modify payment method id ${req.params.id}, body ${req.params.body}`)
  let paymentMethod = await Payment_method.findOne({where: { id: req.params.id }})
  debug(`Found payment method to modify ${JSON.stringify(paymentMethod)}`)
  await paymentMethod.update({...req.body})
  return res.status(200).send({paymentMethod})
}

async function deletePaymentMethod(req, res) {

  debug(`Deleting payment method id: ${req.params.id}`)
  let paymentMethod = await Payment_method.findOne({where: { id: req.params.id }})
  debug(`Payment method to be deleted: ${JSON.stringify(paymentMethod)}`)
  await paymentMethod.destroy()
  return res.status(204).send()
  
}

module.exports = {
  createPaymentMethod,
  getAllPaymentMethods,
  modifyPaymentMethod,
  deletePaymentMethod
}