import Debug from 'debug'
const debug = Debug('APP:auth.controller')

import { User } from '../models'

exports.login = async(req, res) => {
  debug(`Loggin the user: ${JSON.stringify(req.body)}`)
  try {
    let { username, password } = req.body
    let user =  await User.autheticate(username, password)
    debug(`User logged in ${JSON.stringify(user)}`)
    return res.status(200).json(user)
  }catch(error) {
    return res.status(404).send({"errors": [
        {
          "msg": error.message
        }
    ]})
  }
}

exports.signup = async(req, res) => {
  debug(`Registering the user ${JSON.stringify(req.body)}`)
  try {
    let user = await User.create({...req.body})
    return res.status(201).send(user)
  } catch(error) {
    debug(error)
    return res.status(400).json({errors: error})
  }
}