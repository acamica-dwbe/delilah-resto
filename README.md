# Delilah Restó
#### _Proyecto para el curso de desarrollo web back end de Acamica_

Aqui encontrará las instrucciones para correr este proyecto de manera local

## Installation

Delilah requiere [Node.js](https://nodejs.org/) v12+ to run.

Instalar las dependencias y correr el servidor.:

```sh
git clone https://gitlab.com/acamica-dwbe/delilah-resto
cd delilah-resto
npm i
npm run start-dev
```

Para ingresar a la documentacion hecha en swagger: 
```sh
http://localhost:3000/api-docs/
```
Usuarios de prueba
```sh
Admin
- username: admin
- password: 123456
No Admin
- username: camilo
- password: 123456
```

## License

MIT
