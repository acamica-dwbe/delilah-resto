require('dotenv').config()
import compression from 'compression'
import express from 'express'
import cors from 'cors'
import { LOGGER } from '../middlewares'


const app = express()
// app.use(cors())
app.use(compression())
app.use(express.json())

// Custom midlewares
app.use(LOGGER)
// default app configuration
require('../config/app.config')(app)


// -- swagger
// const swaggerJsDoc = require('swagger-jsdoc');
// const swaggerUI = require('swagger-ui-express');

// const swaggerDefinition = {
//   openapi: '3.0.0',
//   info: {
//     title: 'API para el proyecto Delilah Resto',
//     version: '2.1.0',
//     description:
//       'REST API hecha con express.js',
//     license: {
//       name: 'Licensed Under MIT',
//       url: 'https://spdx.org/licenses/MIT.html',
//     }
//   },
//   servers: [
//     {
//       url: 'http://localhost:3000',
//       description: 'Servidor de desarrollo',
//     },
//   ],
// }

// const swaggerOptions = {
//   swaggerDefinition,
//   basicAuth: {
//     name:   'Authorization',
//     schema: {
//       type: 'basic',
//       in:   'header'
//     },
//     value:  'Basic <username:password>'
//   },
//   apis: ['./routes/index.js'],
// }

// const swaggerDocs = swaggerJsDoc(swaggerOptions)

var swaggerUi = require('swagger-ui-express')
var fs = require('fs')
var jsyaml = require('js-yaml');
var spec = fs.readFileSync('./swagger.yml', 'utf8');
var swaggerDocument = jsyaml.safeLoad(spec);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// app.use('/api-docs',
//   swaggerUI.serve,
//   swaggerUI.setup(swaggerDocs)
// )

// Version 1 of the api
app.use('/api/v1', require('../routes'))

module.exports = app

