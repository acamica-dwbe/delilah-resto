'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('payment_methods', [{
      type: 'Tarjeta de Cédito',
      createdAt: 'Sat May 15 2021',
      updatedAt: 'Sat May 15 2021'
    }, {
      type: 'Efectivo',
      createdAt: 'Sat May 15 2021',
      updatedAt: 'Sat May 15 2021'
    }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('payment_methods', null, {})
  }
};
