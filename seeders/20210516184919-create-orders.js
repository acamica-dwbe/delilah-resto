'use strict';

const { sequelize } = require("../models");

function randomElement(array) {
  return array[Math.floor(Math.random() * array.length)]
}

module.exports = {
  up: async (queryInterface, Sequelize) => {

    let paymentMethods = await queryInterface.sequelize.query('SELECT * from "payment_methods"', {
      type: queryInterface.sequelize.QueryTypes.SELECT
    })

    let users = await queryInterface.sequelize.query('SELECT * from "users" WHERE is_admin=false', {
      type: queryInterface.sequelize.QueryTypes.SELECT
    })

    let products = await queryInterface.sequelize.query('SELECT * from "products"' , {
      type: queryInterface.sequelize.QueryTypes.SELECT
    })

    for (const index in users) {
      let user = users[index]
      await queryInterface.bulkInsert('orders', [{
        id_payment_method: randomElement(paymentMethods).id,
        user_id: user.id,
        total: 0,
        estado: "pendiente",
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString()
      }])
    }

    let orders = await queryInterface.sequelize.query('SELECT * from "orders"' , {
      type: queryInterface.sequelize.QueryTypes.SELECT
    })

    for (const order of orders) {
      // Add products to the order
      let randomNumberOfProducts =  Math.floor(Math.random() * products.length)
      let productsToAdd = products.slice(0, randomNumberOfProducts > 0 ?randomNumberOfProducts : 1 )
      let total = 0
      for(const product of productsToAdd) {
        let quantity = Math.floor(Math.random() * 10) + 1 // Entre 1 y 11
        total += (product.price * quantity)
        await queryInterface.bulkInsert('product_orders', [{
          product_id: product.id,
          order_id: order.id,
          quantity: quantity,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString()
        }])
      }
      await queryInterface.sequelize.query(`UPDATE orders SET total=${total} WHERE id=${order.id}`)
    }

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('orders', null, {})
    await queryInterface.bulkDelete('product_orders', null, {})
  }
};
