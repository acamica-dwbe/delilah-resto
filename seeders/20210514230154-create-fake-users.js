'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('users', [{
      name: "Camilo",
      is_admin: false,
      email: "cposadaa@gmail.com",
      password: "123456",
      telephone: "555-555-555",
      address: "Calle 10 numero 25",
      username: "camilo",
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    },{
      name: "Pepito",
      is_admin: false,
      email: "pepito@perez.com",
      password: "123456",
      telephone: "555-555-555",
      address: "Calle 10 numero 25",
      username: "pepito",
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    },{
      name: "Admin",
      is_admin: true,
      email: "admin@gmail.com",
      password: "123456",
      telephone: "555-555-555",
      address: "Calle 10 numero 25",
      username: "admin",
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {})
  }
};
