'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('products', [{
      quantity: 100,
      price: 1000,
      name: 'Zanahoria',
      category: 'Vegetales',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    },{
      quantity: 100,
      price: 2000,
      name: 'Aguacate',
      category: 'Vegetales',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    },{
      quantity: 100,
      price: 20000,
      name: 'Solomo',
      category: 'Carnes',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    },{
      quantity: 100,
      price: 1000,
      name: 'Sal',
      category: 'Condimentos',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    },{
      quantity: 100,
      price: 1000,
      name: 'Azucar',
      category: 'Condimentos',
      createdAt: new Date().toDateString(),
      updatedAt: new Date().toDateString()
    }])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('products', null, {})
  }
};
